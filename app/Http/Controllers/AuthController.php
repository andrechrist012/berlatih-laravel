<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('register');
    }

    public function welcome(Request $request){
        $firstName = $request['FirstName'];
        $lastName = $request['LastName']; 
        return view('welcome', compact('firstName', 'lastName'));
    }

}
