<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="">First name:</label>
        <br><br>
        <input type="text" name="FirstName">
        <br><br>
        <label for="">Last name:</label>
        <br><br>
        <input type="text" name="LastName">
        <br>
        <p>Gender:</p>
        <input type="radio" name="gender" value="Male">
        <label for="html">Male</label><br>
        <input type="radio" name="gender" value="Female">
        <label>Female</label><br>
        <input type="radio" name="gender" value="Other">
        <label>Other</label>
        <p>Nationality</p>
        <select name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="USA">USA</option>
            <option value="Japan">Japan</option>
        </select>
        <p>Languae spoken:</p>
        <input type="checkbox" name="Languae" value="Indonesia">
        <label> Bahasa Indonesia</label><br>
        <input type="checkbox" name="Languae1" value="English">
        <label>English</label><br>
        <input type="checkbox" name="Languae2" value="Other">
        <label>Other</label>
        <p>Bio:</p>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea>
        <br>
        <button>Sign Up</button>
    </form>
</body>
</html>